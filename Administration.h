#ifndef _ADMINISTRATION_H_
#define _ADMINISTRATION_H_


#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitSuite(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);

// Open a document view
extern "C" AFX_EXT_API void OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);

// Open a document view with argument. Added 2012-06-25 P�D
extern "C" AFX_EXT_API void OpenSuiteArg(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,LPCTSTR arg1, int *ret);

#endif