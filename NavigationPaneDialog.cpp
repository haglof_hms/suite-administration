// NavigationPaneDialog.cpp : implementation file
//

#include "stdafx.h"
#include "NavigationPaneDialog.h"

#include "ResLangFileReader.h"
#include ".\navigationpanedialog.h"


// CNavigationPaneDialog

IMPLEMENT_DYNCREATE(CNavigationPaneDialog, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CNavigationPaneDialog, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_MOVE_UP, OnBnClickedBtnMoveUp)
	ON_BN_CLICKED(IDC_BTN_MOVE_DOWN, OnBnClickedBtnMoveDown)
END_MESSAGE_MAP()

CNavigationPaneDialog::CNavigationPaneDialog()
	: CXTResizeFormView(CNavigationPaneDialog::IDD)
{
}

CNavigationPaneDialog::~CNavigationPaneDialog()
{
}

void CNavigationPaneDialog::OnDestroy()
{
	CString sText;
	CString sSuiteXML;
	CString S;
	CShellTreeInfo *rec;
	if (m_bIsDirty)
	{

		for (int i = 0;i < m_wndCheckLB.GetCount();i++)
		{
			m_wndCheckLB.GetText(i,sText);
			rec = (CShellTreeInfo *)m_wndCheckLB.GetItemData(i);
			m_sarrSuitesList.Add(extractFileNameNoExt(rec->getSuitePath()));
		}
		setShellDataFiles(m_sarrSuitesList);

		SEND_MESSAGE_DATA_TRANS_STRUCT data;
		_tcscpy((wchar_t*)data.szValue,m_sAbrevLangSet);
		data.nCommand = ID_CHANGE_LANGUAGE;
		
		// Send message to Main windows (CWinApp); 051205 p�d
		AfxGetMainWnd()->SendMessage(WM_COMMAND,CMDID_CHANGE_LANGUAGE,0);

	}
	CXTResizeFormView::OnDestroy();
}

void CNavigationPaneDialog::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP1, m_wndGroup);

	DDX_Control(pDX, IDC_LIST2, m_wndCheckLB);

	DDX_Control(pDX, IDC_STATIC1, m_wndLbl1);

	DDX_Control(pDX, IDC_BTN_MOVE_DOWN, m_wndBtnMoveDown);
	DDX_Control(pDX, IDC_BTN_MOVE_UP, m_wndBtnMoveUp);
	//}}AFX_DATA_MAP

}


void CNavigationPaneDialog::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	SetFlag(xtResizeNoTransparentGroup);

	m_sAbrevLangSet = getLangSet();

	m_bIsDirty = FALSE;
	
	// Get ShellData files in Shortcuts.xml; 051115 p�d
	getShellDataFiles(m_arrShellDataFileList);
	getShellTreeInfo(m_arrShellDataFileList,m_vecSTInfo);

	setLanguage();

	setupSuites();
}

BOOL CNavigationPaneDialog::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CNavigationPaneDialog diagnostics

#ifdef _DEBUG
void CNavigationPaneDialog::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CNavigationPaneDialog::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CNavigationPaneDialog message handlers

// PRIVATE
void CNavigationPaneDialog::setLanguage(void)
{
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING101));
			m_wndBtnMoveUp.SetWindowText(xml->str(IDS_STRING102));
			m_wndBtnMoveDown.SetWindowText(xml->str(IDS_STRING103));
		}
		delete xml;
	}
}

void CNavigationPaneDialog::setupSuites(void)
{
	CString sSuiteFileName;
	CString sSuiteName;
	CShellTreeInfo *rec;
	CString S;

	for (UINT j = 0;j < m_vecSTInfo.size();j++)
	{
		rec = new CShellTreeInfo( m_vecSTInfo[j]);
		m_wndCheckLB.AddString(rec->getCaption());
		m_wndCheckLB.SetItemData(j,DWORD_PTR(rec));
	}

}

// EVENTS
void CNavigationPaneDialog::OnBnClickedBtnMoveUp()
{
	CShellTreeInfo *rec;
	CString sText;
	int nIndex = m_wndCheckLB.GetCurSel();
	if (nIndex > 0)
	{
		m_wndCheckLB.GetText(nIndex,sText);
		rec = (CShellTreeInfo *)m_wndCheckLB.GetItemData(nIndex);
		m_wndCheckLB.DeleteString(nIndex);
		m_wndCheckLB.InsertString(nIndex-1,sText);
		m_wndCheckLB.SetItemData(nIndex-1,DWORD_PTR(rec));
	}
	m_wndCheckLB.SelectString(0,sText);
	m_bIsDirty = TRUE;

}

void CNavigationPaneDialog::OnBnClickedBtnMoveDown()
{
	CShellTreeInfo *rec;
	CString sText;
	int nIndex = m_wndCheckLB.GetCurSel();
	int nNumOf = m_wndCheckLB.GetCount();
	if (nIndex < (nNumOf - 1))
	{
		m_wndCheckLB.GetText(nIndex,sText);
		rec = (CShellTreeInfo *)m_wndCheckLB.GetItemData(nIndex);
		m_wndCheckLB.DeleteString(nIndex);
		m_wndCheckLB.InsertString(nIndex+1,sText);
		m_wndCheckLB.SetItemData(nIndex+1,DWORD_PTR(rec));
	}
	m_wndCheckLB.SelectString(0,sText);
	m_bIsDirty = TRUE;
}
