#pragma once


///////////////////////////////////////////////////////////////////////////////////////////
// CMDIReportDoc

class CMDIReportDoc : public CDocument
{
protected: // create from serialization only
	CMDIReportDoc();
	DECLARE_DYNCREATE(CMDIReportDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIReportDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIReportDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIReportDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
///////////////////////////////////////////////////////////////////////////////////////////
// CMDINewsBulletineFrame frame

class CMDINewsBulletineFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDINewsBulletineFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDINewsBulletineFrame();           // protected constructor used by dynamic creation
	virtual ~CMDINewsBulletineFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDINavigationWindowFrame frame

class CMDINavigationWindowFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDINavigationWindowFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDINavigationWindowFrame();           // protected constructor used by dynamic creation
	virtual ~CMDINavigationWindowFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDISuitesUserModulesFrame frame

class CMDISuitesUserModulesFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDISuitesUserModulesFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDISuitesUserModulesFrame();           // protected constructor used by dynamic creation
	virtual ~CMDISuitesUserModulesFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CUserReportsFrame frame

class CUserReportsFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CUserReportsFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipImport;
	CString m_sToolTipPrintOut;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CUserReportsFrame();           // protected constructor used by dynamic creation
	virtual ~CUserReportsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	CDialogBar m_wndFieldChooser;   // Sample Field chooser window
	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg void OnClose();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

