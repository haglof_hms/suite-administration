// stdafx.cpp : source file that includes just the standard includes
// Adminstration.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "XMLHandler.h"


CString getResStr(UINT id)
{
	CString sText;
	XTPResourceManager()->LoadString(&sText, id);

	return sText;
}

BOOL getRSSXMLFile(CStringArray &str_array,long *size)
{
	// FTP get RSS xml file from the net; 060801 p�d
	W3Client w3;
	long lSize = 0;
	CString sBuffer;
	TCHAR szRSS_Url[128],szRSS_FilePath[MAX_PATH];

	getRSSLocation(szRSS_Url,szRSS_FilePath);

	if(w3.Connect(szRSS_Url))
	{
    if(w3.Request(szRSS_FilePath))
		{
			int nRet;
    	char buf[1024] = "\0";
    	while (nRet = w3.Response(reinterpret_cast<unsigned char *>(buf), 1024))
			{
				buf[nRet] = '\0';
				sBuffer += buf;
				lSize += nRet;
			}

    }
    w3.Close();
		*size = lSize;
		str_array.RemoveAll();	// Clear list
		str_array.Add(sBuffer);
	}
	return (str_array.GetCount() > 0);
}

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


RSSXMLFilePars::RSSXMLFilePars(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

RSSXMLFilePars::~RSSXMLFilePars()
{
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL RSSXMLFilePars::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL RSSXMLFilePars::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL RSSXMLFilePars::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString RSSXMLFilePars::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double RSSXMLFilePars::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

BOOL RSSXMLFilePars::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

// Define nodes in RSS XML-file; 060801 p�d
#define NODE_ROOT						_T("item")
#define NODE_LASTBUILD_TITLE			_T("title")
#define NODE_LASTBUILD_LINK				_T("link")
#define NODE_LASTBUILD_DESC				_T("description")
#define NODE_LASTBUILD_CATEGORY			_T("category")
#define NODE_LASTBUILD_PUB_DATE			_T("pubDate")
#define NODE_LASTBUILD_PUB_DATE0		_T("//channel/item/pubDate")

// Methods for reading Header information
BOOL RSSXMLFilePars::getThisPubDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_LASTBUILD_PUB_DATE0);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}

	return FALSE;
}

BOOL RSSXMLFilePars::getNewsBulletines(vecNewsBulletineItems &vec)
{
	CComBSTR bstrTitle;
	CComBSTR bstrLink;
	CComBSTR bstrDesc;
	CComBSTR bstrCategory;
	CComBSTR bstrPubDate;
	long lNumOf;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_ROOT));
	MSXML2::IXMLDOMNodePtr pNode = NULL;
	if (pNodeList)
	{	
		vec.clear();
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_TITLE));
				if (pNode)
				{
					pNode->get_text(&bstrTitle);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_LINK));
				if (pNode)
				{
					pNode->get_text(&bstrLink);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_DESC));
				if (pNode)
				{
					pNode->get_text(&bstrDesc);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_CATEGORY));
				if (pNode)
				{
					pNode->get_text(&bstrCategory);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_PUB_DATE));
				if (pNode)
				{
					pNode->get_text(&bstrPubDate);
				}

				vec.push_back(CNewsBulletineItem(CString(bstrTitle),
																				 CString(bstrLink),
																				 CString(bstrDesc),
																				 CString(bstrCategory),
																				 CString(bstrPubDate)));
			}	// if (pChild)
		}	// for (long l = 0;l < lNumOf;l++)
		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}


BOOL RSSXMLFilePars::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}

CString getShellDataDir(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 
	{
		sPath.Format(_T("%s\\%s"),szPath,SUBDIR_SHELL_DATA);
	}
	return sPath;
}


BOOL getShellDataFiles(CStringArray &str_list)
{
	CString sSuitePath;
	CString sFile;
	BOOL bFileExists,bItemsInList;

	bFileExists = bItemsInList = FALSE;
	str_list.RemoveAll();	// Clear list

	sSuitePath.Format(_T("%s\\%s"),getShellDataDir(),SHELLDATA_ShortcutsFN);
	// Get suites to be added to Navigation bar, from Shortcuts.xml; 060612 p�d
	XMLHandler *xml = new XMLHandler();
	if (fileExists(sSuitePath))
	{
		if (xml)
		{
			if (xml->load(sSuitePath))
			{
				// Get all ShellData files in ShellData subdirectory; 051115 p�d
				xml->getSuitesInShellTree(str_list);
			}	// if (xml->load(sSuitePath))
		}	// if (xml)
	}	// if (fileExists(sSuitePath))
	delete xml;

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;

}


BOOL setShellDataFiles(CStringArray &str_list)
{
	CString sSuitePath;
	CString sFile;
	BOOL bFileExists,bItemsInList;

	bFileExists = bItemsInList = FALSE;
	sSuitePath.Format(_T("%s\\%s"),getShellDataDir(),SHELLDATA_ShortcutsFN);
	// Get suites to be added to Navigation bar, from Shortcuts.xml; 060612 p�d
	XMLHandler *xml = new XMLHandler();
	if (fileExists(sSuitePath))
	{
		if (xml)
		{
			if (xml->load(sSuitePath))
			{
				// Get all ShellData files in ShellData subdirectory; 051115 p�d
				xml->setSuitesInShellTree(str_list);
			}	// if (xml->load(sSuitePath))
		}	// if (xml)
		xml->save(sSuitePath);
	}	// if (fileExists(sSuitePath))
	delete xml;

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;

}

BOOL getShellTreeInfo(CStringArray &shellfiles_list,	// Array of files in Shortcuts.xml
										  vecShellTree &shelltree_list)		// ShellTree data
{
	XMLHandler *xml = new XMLHandler();
	// Check if there's any ShellData files; 051115 p�d
	if (shellfiles_list.GetCount() > 0)
	{

		for (int i = 0;i < shellfiles_list.GetCount();i++)
		{
			if (xml->load(shellfiles_list.GetAt(i)))
			{
				xml->getShellTreeInfo(shelltree_list);
			}	// if (xml->load(m_arrShellDataFileList.GetAt(i)))
		}	// for (int i = 0;i < m_arrShellDataFileList.GetCount();i++)
	}	// if (m_arrShellDataFileList.GetCount() > 0)
	delete xml;

	return TRUE;
}

CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}


CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}


//CReportMultilinePaintManager
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CReportMultilinePaintManager::CReportMultilinePaintManager()
{
	m_bFixedRowHeight = FALSE;
}

CReportMultilinePaintManager::~CReportMultilinePaintManager()
{

}

int CReportMultilinePaintManager::GetRowHeight(CDC* pDC, CXTPReportRow* pRow, int nTotalWidth)
{
	if (pRow->IsGroupRow() || !pRow->IsItemsVisible())
		return CXTPReportPaintManager::GetRowHeight(pDC, pRow, nTotalWidth);

	CXTPReportColumns* pColumns = pRow->GetControl()->GetColumns();
	int nColumnCount = pColumns->GetCount();

	XTP_REPORTRECORDITEM_DRAWARGS drawArgs;
	drawArgs.pControl = pRow->GetControl();
	drawArgs.pDC = pDC;
	drawArgs.pRow = pRow;

	int nHeight = 0;

	for (int nColumn = 0; nColumn < nColumnCount; nColumn++)
	{
		CXTPReportColumn* pColumn = pColumns->GetAt(nColumn);
		if (pColumn && pColumn->IsVisible())
		{
			CXTPReportRecordItem* pItem = pRow->GetRecord()->GetItem(pColumn);
			drawArgs.pItem = pItem;

			XTP_REPORTRECORDITEM_METRICS itemMetrics;
			pRow->GetItemMetrics(&drawArgs, &itemMetrics);

			CXTPFontDC fnt(pDC, itemMetrics.pFont);

			int nWidth = pDC->IsPrinting()? pColumn->GetPrintWidth(nTotalWidth): pColumn->GetWidth();

			CRect rcItem(0, 0, nWidth - 4, 0);
			pRow->ShiftTreeIndent(rcItem, pColumn);

			pItem->GetCaptionRect(&drawArgs, rcItem);
			pDC->DrawText(pItem->GetCaption(pColumn), rcItem, DT_WORDBREAK|DT_CALCRECT);

			nHeight = max(nHeight, rcItem.Height());
		}
	}


	return max(nHeight + 5, m_nRowHeight) + (IsGridVisible(FALSE)? 1: 0);
}

void CReportMultilinePaintManager::DrawItemCaption(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pMetrics)
{
	CRect& rcItem = pDrawArgs->rcItem;
	CDC* pDC = pDrawArgs->pDC;
	CString strText = pMetrics->strText;

	// draw item text
	if(!strText.IsEmpty())
	{
		rcItem.DeflateRect(2, 1, 2, 0);
		pDC->DrawText(strText, rcItem, pDrawArgs->nTextAlign|DT_WORDBREAK);
	}
}

