#include "stdafx.h"
#include "ModuleInfoParser.h"

/////////////////////////////////////////////////////////////////////////////////////
// CAdminSuiteModuleInfoParser

// Handle Suite and User module info into XML file; 060803 p�d

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

CAdminSuiteModuleInfoParser::CAdminSuiteModuleInfoParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

CAdminSuiteModuleInfoParser::~CAdminSuiteModuleInfoParser()
{
  CoUninitialize();
}

BOOL CAdminSuiteModuleInfoParser::CreateDoc(void)
{
	_variant_t vNullVal;
	MSXML2::IXMLDOMProcessingInstructionPtr pPI = NULL;
	//---------------------------------------------------------------------------
	// Create an empty DOM; 030120 p�d
	//---------------------------------------------------------------------------
	pDomDoc->loadXML(_T(""));
	//---------------------------------------------------------------------------
	// Add the XML processing instruction ("<?xml version="1.0"?>"); 030120 p�d
	//---------------------------------------------------------------------------

	pPI = pDomDoc->createProcessingInstruction(PI_XML_NAME, PI_VER_ENCODE);

	vNullVal.vt = VT_NULL;
	pDomDoc->insertBefore(pPI, vNullVal);

	return true;
}

// Methods for Create,Load and Saving xml file(s); 060407 p�d
BOOL CAdminSuiteModuleInfoParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL CAdminSuiteModuleInfoParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL CAdminSuiteModuleInfoParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
MSXML2::IXMLDOMNodePtr CAdminSuiteModuleInfoParser::addItem(MSXML2::IXMLDOMNodePtr node,LPCTSTR S)
{
	node->text = _bstr_t(S);
	return node;
}

// Public methods

BOOL CAdminSuiteModuleInfoParser::setSuiteModuleInfo(vecINFO_TABLE &vec)
{
	CString sStrID;
	MSXML2::IXMLDOMNodePtr ptrNodeSuite = NULL;
	MSXML2::IXMLDOMNodePtr ptrNodeUM = NULL;
	MSXML2::IXMLDOMNodePtr ptrItem = NULL;
	// Setup ROOT
	MSXML2::IXMLDOMElementPtr ptrRootElem = pDomDoc->createElement(TAG_ROOT);
	pDomDoc->documentElement = ptrRootElem;
	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pDomDoc->documentElement;
	
	// Add element to Suite node
	for (UINT i = 0;i < vec.size();i++)
	{
		_info_table rec = vec[i];
		sStrID.Format(_T("%d"),rec.nStrIndex);
		// Add Suite(s); 060803 p�d
		if (rec.nType == 1)
		{
			MSXML2::IXMLDOMElementPtr ptrElem = pDomDoc->createElement(TAG_SUITE);
			ptrNodeSuite = ptrRoot->appendChild( ptrElem );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_STRID ),sStrID) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_LANG_FILE ),rec.szLanguageFN) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_VERSION ),rec.szVersion) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COPYRIGHT ),rec.szCopyright) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COMPANY ),rec.szCompany) );
		}	// if (rec.nType == 1)

		// Add User module(s) in Suite; 060804 p�d
		if (rec.nType == 2)
		{
			MSXML2::IXMLDOMElementPtr ptrElem = pDomDoc->createElement(TAG_USER_MODULE);
			ptrNodeUM = ptrNodeSuite->appendChild( ptrElem );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_STRID ),sStrID) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_LANG_FILE ),rec.szLanguageFN) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_VERSION ),rec.szVersion) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COPYRIGHT ),rec.szCopyright) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COMPANY ),rec.szCompany) );
		}	// if (rec.nType == 2)

	} // for (UINT i = 0;i < vec.size();i++)


	return TRUE;
}

BOOL CAdminSuiteModuleInfoParser::getSuiteModuleInfo(vecINFO_TABLE &vec)
{
	TCHAR szStrID[50];
	TCHAR szLangFile[MAX_PATH];
	TCHAR szVersion[MAX_PATH];
	TCHAR szCopyRight[MAX_PATH];
	TCHAR szCompany[MAX_PATH];
	CString S;
	MSXML2::IXMLDOMNodePtr pSuiteChild = NULL;
	MSXML2::IXMLDOMNodePtr pUMChild = NULL;
	MSXML2::IXMLDOMNodePtr pSuiteElem = NULL;
	MSXML2::IXMLDOMNodePtr pUMElem = NULL;
	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pDomDoc->documentElement;

	// Get suite(s) in file; 060811 p�d
	MSXML2::IXMLDOMNodeListPtr pSuiteNodeList = pDomDoc->selectNodes(TAG_ROOT_WC);
	if (pSuiteNodeList != NULL)
	{
		for (long i = 0;i < pSuiteNodeList->length;i++)
		{
			pSuiteChild = pSuiteNodeList->item[i];
			if (pSuiteChild != NULL)
			{
					// Get data for suite; 060811 p�d
					pSuiteElem = pSuiteChild->selectSingleNode(TAG_ELEM_STRID);
					if (pSuiteElem)
						_tcscpy(szStrID,pSuiteElem->text);

					pSuiteElem = pSuiteChild->selectSingleNode(TAG_ELEM_LANG_FILE);
					if (pSuiteElem)
						_tcscpy(szLangFile,pSuiteElem->text);

					pSuiteElem = pSuiteChild->selectSingleNode(TAG_ELEM_VERSION);
					if (pSuiteElem)
						_tcscpy(szVersion,pSuiteElem->text);

					pSuiteElem = pSuiteChild->selectSingleNode(TAG_ELEM_COPYRIGHT);
					if (pSuiteElem)
						_tcscpy(szCopyRight,pSuiteElem->text);

					pSuiteElem = pSuiteChild->selectSingleNode(TAG_ELEM_COMPANY);
					if (pSuiteElem)
						_tcscpy(szCompany,pSuiteElem->text);

					vec.push_back(INFO_TABLE(_tstoi(szStrID),1,szLangFile,szVersion,szCopyRight,szCompany));

					if (pSuiteElem->hasChildNodes())
					{

						MSXML2::IXMLDOMNodeListPtr pUMNodeList = pSuiteChild->selectNodes(TAG_USER_MODULE);
						if (pUMNodeList)
						{
							for (long j = 0;j < pUMNodeList->length;j++)
							{
									pUMChild = pUMNodeList->item[j];
									if (pUMChild)
									{
										// Get data for suite; 060811 p�d
										pUMElem = pUMChild->selectSingleNode(TAG_ELEM_STRID);
										if (pUMElem)
											_tcscpy(szStrID,pUMElem->text);

										pUMElem = pUMChild->selectSingleNode(TAG_ELEM_LANG_FILE);
										if (pUMElem)
											_tcscpy(szLangFile,pUMElem->text);

										pUMElem = pUMChild->selectSingleNode(TAG_ELEM_VERSION);
										if (pUMElem)
											_tcscpy(szVersion,pUMElem->text);

										pUMElem = pUMChild->selectSingleNode(TAG_ELEM_COPYRIGHT);
										if (pUMElem)
											_tcscpy(szCopyRight,pUMElem->text);

										pUMElem = pUMChild->selectSingleNode(TAG_ELEM_COMPANY);
										if (pUMElem)
											_tcscpy(szCompany,pUMElem->text);

										vec.push_back(INFO_TABLE(_tstoi(szStrID),2,szLangFile,szVersion,szCopyRight,szCompany));

									}	// if (pUMChild)
							} // for (long j = 0;j < pUMNodeList->length;j++)

						}	// if (pUMNodeList)

					}	// if (pSuiteChild->hasChildNodes)
				} // if (pSuiteChild)
	
		}	// for (long i = 0;i < pSuiteNodeList->length;i++)
	}	// 	if (pSuiteNodeList != NULL)

	

	return TRUE;
}



BOOL CAdminSuiteModuleInfoParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}
