#pragma once

#include "Resource.h"

#define IDC_PROP_GRID 100
//////////////////////////////////////////////////////////////////////////
// CMyPropertyGridItemBool derived from CXTPPropertyGridItemBool
class CMyPropertyGridItemBool : public CXTPPropertyGridItemBool
{
public:
	CMyPropertyGridItemBool(const CString true_text,
													const CString false_text,
													const CString& cap,
													BOOL bValue) : CXTPPropertyGridItemBool(cap,bValue)
	{
		SetTrueFalseText((true_text),(false_text));
	}
};
//////////////////////////////////////////////////////////////////////////
// CPropertyGridItemExchange

class CPropertyGridItemExchange : public CXTPPropertyGridItem
{
//private:
	CString m_sValue;
public:
	CPropertyGridItemExchange(CString strCaption,CString *strRetVal);

	~CPropertyGridItemExchange(void);

protected:
	virtual BOOL OnDrawItemValue(CDC& dc, CRect rcValue);
	virtual void OnInplaceButtonDown();
};

// CNewsBulletineDlg form view

class CNewsBulletineDlg : public CXTResizeFormView //CFormView
{
	DECLARE_DYNCREATE(CNewsBulletineDlg)

	void setLanguage(void);

	BOOL excludeCategory(LPCTSTR);

	void saveNewsCatergoriesSetup(void);
	void getNewsCatergoriesSetup(void);

	vecNewsCategoriesSetup m_vecNewsCategories;

	TCHAR m_szPermanentCategories[128];

	TCHAR* CNewsBulletineDlg::rtrim(TCHAR* string, TCHAR junk);
	TCHAR* CNewsBulletineDlg::ltrim(TCHAR *string, TCHAR junk);

protected:
	CNewsBulletineDlg();           // protected constructor used by dynamic creation
	virtual ~CNewsBulletineDlg();

	void setupCategories(void);

	CXTResizeGroupBox m_wndGroup;
	CStatic m_wndLbl1;

	CXTPPropertyGrid m_wndPropertyGrid;

	CString m_sExchangeTemplate;
	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CString m_sTrueCap;
	CString m_sFalseCap;

	BOOL m_bInitialized;
public:
	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


