// SuitesUserModulesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ModuleInfoParser.h"
#include "SuitesUserModulesDlg.h"
#include "ResLangFileReader.h"
#include "XMLHandler.h"
// CSuitesUserModulesDlg

IMPLEMENT_DYNCREATE(CSuitesUserModulesDlg, CXTResizeFormView)


BEGIN_MESSAGE_MAP(CSuitesUserModulesDlg, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CSuitesUserModulesDlg::CSuitesUserModulesDlg()
	: CXTResizeFormView(CSuitesUserModulesDlg::IDD)
{
	m_bInitialized = FALSE;
}

CSuitesUserModulesDlg::~CSuitesUserModulesDlg()
{
}

void CSuitesUserModulesDlg::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}


void CSuitesUserModulesDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

void CSuitesUserModulesDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	SetFlag(xtResizeNoTransparentGroup);

	if (! m_bInitialized )
	{
		// Get configuration xml-file for
		// Suites and User modules; 060811 p�d
		getSuiteUserModuleConfig();

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport1();
		
		m_bInitialized = TRUE;

	}
}

BOOL CSuitesUserModulesDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CSuitesUserModulesDlg diagnostics

#ifdef _DEBUG
void CSuitesUserModulesDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSuitesUserModulesDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CSuitesUserModulesDlg message handlers



BOOL CSuitesUserModulesDlg::setupReport1(void)
{
	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_REPORT1))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				CXTPReportColumn *pCol1 = m_wndReport1.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING104)), 50));
				pCol1->GetEditOptions()->m_bAllowEdit = FALSE;
				CXTPReportColumn *pCol2 = m_wndReport1.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING105)), 150));
				pCol2->GetEditOptions()->m_bAllowEdit = FALSE;

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_REPORT1),2,2,rect.right - 4,rect.bottom - 4);

				m_wndReport1.FocusSubItems(TRUE);
			}
			delete xml;
		}	// if (fileExists(sLangFN))


	}

	return TRUE;

}
void CSuitesUserModulesDlg::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	// Need to set size of Report control; 051219 p�d
	RECT rect;
	GetClientRect(&rect);
	setResize(GetDlgItem(IDC_REPORT1),2,2,rect.right - 4,rect.bottom - 4);

}

// PRIVATE
void CSuitesUserModulesDlg::getSuiteUserModuleConfig(void)
{
	CString sSuiteModulePath;
	sSuiteModulePath = getSuiteModuleConfigPathAndFile();
	CAdminSuiteModuleInfoParser *p = new CAdminSuiteModuleInfoParser;
	if (p)
	{
		if (p->LoadFromFile(sSuiteModulePath))
		{
			m_vecInfoTable.clear();
			p->getSuiteModuleInfo( m_vecInfoTable);
		}

	}
	delete p;
}

BOOL CSuitesUserModulesDlg::populateReport(void)
{
	CShellTreeReportRec *pReport = NULL;
	CString sLangFileName;
	CString sName;
	// Add User types to report; 060217 p�d
	if (m_wndReport1.ClearReport())
	{
		if ( m_vecInfoTable.size() > 0)
		{
				for (UINT i = 0;i <  m_vecInfoTable.size();i++)
				{
					INFO_TABLE rec =  m_vecInfoTable[i];
						sLangFileName.Format(_T("%s%s%s"),rec.szLanguageFN,m_sLangAbrev,LANGUAGE_FN_EXT);
						if (fileExists(sLangFileName))
						{
							RLFReader *xml = new RLFReader;
							if (xml->Load(sLangFileName))
							{
								sName = xml->str(rec.nStrIndex);
								
								if (rec.nType == 1)
								{
							
									pReport = new CShellTreeReportRec(CShellTreeRec(rec.nType,sName,rec.szVersion,rec.szCopyright,rec.szCompany));
									m_wndReport1.AddRecord(pReport);
								}
								if (rec.nType == 2)
								{
									pReport->GetChilds()->Add(new CShellTreeReportRec(CShellTreeRec(rec.nType,sName,rec.szVersion,rec.szCopyright,rec.szCompany)));
								}
							}	// if (xml->Load(sLangFileName))
						}	// if (fileExists(sLangFileName))
				}	// for (UINT i = 0;i < m_vecUserDB.size();i++)
			} // if (userdb_list().size() > 0)

		m_wndReport1.GetColumns()->Find(0)->SetTreeColumn(TRUE);
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		return TRUE;
	}	// if (m_wndReport1.ClearReport())

	return FALSE;
}

