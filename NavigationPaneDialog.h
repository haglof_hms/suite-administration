#pragma once

#include "Resource.h"

// CNavigationPaneDialog form view

class CNavigationPaneDialog : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CNavigationPaneDialog)

	void setLanguage(void);

	void setupSuites(void);

protected:
	CNavigationPaneDialog();           // protected constructor used by dynamic creation
	virtual ~CNavigationPaneDialog();

	CXTResizeGroupBox m_wndGroup;
	CStatic m_wndLbl1;

	CListBox m_wndCheckLB;

	CButton m_wndBtnMoveDown;
	CButton m_wndBtnMoveUp;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	BOOL m_bIsDirty;

	CStringArray m_sarrSuitesList;

	CStringArray m_arrShellDataFileList;
	vecShellTree m_vecSTInfo;

public:
	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnDestroy();
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnMoveUp();
	afx_msg void OnBnClickedBtnMoveDown();
};


