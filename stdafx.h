// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <shfolder.h>
#include <shlobj.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <vector>
#include <map>

// Xtreeme toolkit
#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "w3c.h"
#include "sqlapi.h"
#include "pad_hms_miscfunc.h"					// HMSFuncLib header


// Define set if tcruise reports should use path set in registry


enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14
};


enum ARG_TYPES { 
	NO_VALUE = 0, 
	LOGSCALE_REPORTS = 1, 
	FOREST_REPORTS = 2,					// Includes ALL reports in Forest suite
	ESTIMATE_REPORTS = 3,				// Can be used to explicit estimate
	LANDVALUE_REPORTS = 4,			// Can be used to explicit landvalue
	TIMBERCRUISE_REPORTS = 5	
};

#define MSG_IN_SUITE				 				(WM_USER + 10)		// This identifer's used to send messages internally

//-----------------------------------------------------------------------------------
// Defines for general reporthandling; added 2012-06-25 P�D
#define MSG_OPEN_SUITE_ARG		 							(WM_USER + 11)		// This identifer's used to send messages internally

#define MSG_ID_ARGUMENT											0x92500

#define IDENTITY_FOR_LOGSCALE_ARG						L"LogScale"
#define IDENTITY_FOR_FOREST_ARG							L"Forest"
#define IDENTITY_FOR_ESTIMATE_ARG						L"Estimate"
#define IDENTITY_FOR_LANDVALUE_ARG					L"LandValue"
#define IDENTITY_FOR_TIMBERCRUISE_ARG				L"TC"		// Timbercruise
#define IDENTITY_FOR_TIMBERCRUISE_ARG2			L"TimberCruise"		// Timbercruise
/* ADD MORE IDENTIFERS HERE */
//-----------------------------------------------------------------------------------


const LPCTSTR HMS_INST_DIR								= _T("SOFTWARE\\HaglofManagmentSystem\\Settings");
const LPCTSTR REPORTS_INST_DIR_KEY_LOGSCALE  			= _T("Logscale_Reports");
const LPCTSTR REPORTS_INST_DIR_KEY_TCRUISE  			= _T("TC_Reports");
const LPCTSTR REPORTS_INST_DIR_KEY_FOREST				= _T("Forest_Reports");
const LPCTSTR REPORTS_INST_DIR_KEY_ESTIMATE				= _T("Estimate_Reports");
const LPCTSTR REPORTS_INST_DIR_KEY_LANDVALUE			= _T("Landvalue_Reports");

// String ID(s) for Text
#define IDS_STRING100									100
#define IDS_STRING101									101
#define IDS_STRING102									102
#define IDS_STRING103									103
#define IDS_STRING104									104
#define IDS_STRING105									105
#define IDS_STRING106									106
#define IDS_STRING107									107
#define IDS_STRING108									108

#define IDS_STRING200									200
#define IDS_STRING201									201
#define IDS_STRING202									202
#define IDS_STRING203									203
#define IDS_STRING204									204
#define IDS_STRING205									205
#define IDS_STRING206									206

#define IDS_STRING240									240
#define IDS_STRING241									241
#define IDS_STRING242									242
#define IDS_STRING243									243
#define IDS_STRING244									244

#define IDS_STRING250									250
#define IDS_STRING251									251

#define IDS_STRING300									300
#define IDS_STRING301									301
#define IDS_STRING302									302
#define IDS_STRING303									303

#define IDS_STRING310									310
#define IDS_STRING311									311

#define IDS_STRING10000								10000
#define IDS_STRING10001								10001
#define IDS_STRING10002								10002
#define IDS_STRING10003								10003
#define IDS_STRING10004								10004


#define ID_CHANGE_LANGUAGE						40000						// Change language, also string value
#define CMDID_CHANGE_LANGUAGE					0x8011					// OnCommand, change language

#define IDC_REPORT1										0x9001

#define ID_TBBTN_COLUMNS							0x9014

// ID's for popupmenu in ReportControl; 090514 p�d
#define ID_GROUP_BYTHIS								0x9100
#define ID_SHOW_GROUPBOX							0x9101
#define ID_SHOW_FIELDCHOOSER					0x9102

const LPCTSTR PROGRAM_NAME			  		= _T("Administration");			// Name of suite/module, used on setting Language filename; 051214 p�d

const LPCTSTR SHELLDATA_FN_EXT				= _T(".xml");				// Uses ordinary xm-file

//////////////////////////////////////////////////////////////////////////////////////////
// Window placement registry key; 060809 p�d

const LPCTSTR REG_WP_NEWSBULLETINE_KEY		= _T("Administration\\NewsBulletine\\Placement");
const LPCTSTR REG_WP_NAVIGATIONPANE_KEY		= _T("Administration\\NavigationPane\\Placement");
const LPCTSTR REG_WP_SUTESUSERMODULE_KEY	= _T("Administration\\SuitesUserModules\\Placement");
const LPCTSTR REG_WP_USERREPORTMODULE_KEY	= _T("Administration\\SuitesUserReportModules\\Placement");

const LPCTSTR REG_WP_USERREPORT_SEL_KEY		= _T("Administration\\SuitesUserReportModules\\Report");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060209 p�d

const int MIN_X_SIZE_NEWSBULLETINE	= 325;
const int MIN_Y_SIZE_NEWSBULLETINE	= 350;

const int MIN_X_SIZE_NAVIGATIONPANE	= 330;
const int MIN_Y_SIZE_NAVIGATIONPANE	= 350;

const int MIN_X_SIZE_SUITESUM				= 400;
const int MIN_Y_SIZE_SUITESUM				= 350;

const int MIN_X_SIZE_USERREPORT			= 50;
const int MIN_Y_SIZE_USERREPORT			= 50;

/////////////////////////////////////////////////////////////////////////////
const LPCTSTR TOOLBAR_RES_DLL						= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d

/////////////////////////////////////////////////////////////////////////////
// Directory for User-reports, in directory Reports
const LPCTSTR USER_REPORTS_DIR								= _T("\\HMS\\Reports\\Custom");
const LPCTSTR USER_REPORTS_DIR_LOGSCALE				= _T("\\HMS\\Reports\\LogScale");
const LPCTSTR USER_REPORTS_DIR_FOREST					= _T("\\HMS\\Reports\\Forest");
const LPCTSTR USER_REPORTS_DIR_ESTIMATE				= _T("\\HMS\\Reports\\Estimate");
const LPCTSTR USER_REPORTS_DIR_LANDVALUE			= _T("\\HMS\\Reports\\Landvalue");
const LPCTSTR USER_REPORTS_DIR_TIMBERCRUISE		= _T("\\HMS\\Reports\\TC");

/////////////////////////////////////////////////////////////////////////////
const FMTID PropSetfmtid ={
/* F29F85E0-4FF9-1068-AB91-08002B27B3D9 */
        0xf29f85e0,
        0x4ff9,
        0x1068,
        {0xab, 0x91, 0x08, 0x00, 0x2b, 0x27, 0xb3, 0xd9 }
        };


/////////////////////////////////////////////////////////////////////////////
// CShellTreeRec
class CShellTreeRec
{
//private:
	int m_nType;
	CString m_sName;		// Suite/User module name
	CString m_sVersion;
	CString m_sCopyright;
	CString m_sCompany;
public:
	CShellTreeRec(void)
	{
		m_nType				= -1;
		m_sName				= "";		// SUite/User module name
		m_sVersion		= "";
		m_sCopyright	= "";
		m_sCompany		= "";
	}
	CShellTreeRec(int type,LPCTSTR name,LPCTSTR version,LPCTSTR copyright,LPCTSTR company)
	{
		m_nType				= type;
		m_sName				= name;		// SUite/User module name
		m_sVersion		= version;
		m_sCopyright	= copyright;
		m_sCompany		= company;
	}
	CShellTreeRec(const CShellTreeRec &c)
	{
		*this = c;
	}

	int getType(void)						{ return m_nType; }
	CString getName(void)				{ return m_sName; }
	CString getVersion(void)		{ return m_sVersion; }
	CString getCopyright(void)	{ return m_sCopyright; }
	CString getComapny(void)		{ return m_sCompany; }
};

/////////////////////////////////////////////////////////////////////////////
// CShellTreeReportRec

class CShellTreeReportRec : public CXTPReportRecord
{
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CShellTreeReportRec(void)
	{
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CShellTreeReportRec(CShellTreeRec rec)
	{
		AddItem(new CTextItem((rec.getName())));
		AddItem(new CTextItem((rec.getVersion())));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

//////////////////////////////////////////////////////////////////////////////////
// CShellTreeInfo

#define NO_VALUE	-9999
#define A_VALUE		9999

class CShellTreeInfo
{
private:
	TCHAR szCaption[50];
/*
	TCHAR szIcon[50];
  TCHAR szNodeName[50];
  TCHAR szElement[50];
  short nVisible;
  int nIndex;
  TCHAR szFunc[50];
  TCHAR szSuite[64];
	int nStrID;
	*/
  TCHAR szSuitePath[MAX_PATH];
/*
	short nCheck;	// A check value used e.g. when setting up Navigaton bar, values can be NO_VALUE or A_VALUE; 051124 p�d
	long nCounter;
	TCHAR szItemText[128];
*/
public:
  CShellTreeInfo()
  {
		_tcscpy(szCaption,_T(""));
/*
		_tcscpy(szIcon,_T(""));
		_tcscpy(szNodeName,_T(""));
		_tcscpy(szElement,_T(""));
		nVisible		= 1;
		nIndex			= -1;
		_tcscpy(szFunc,_T(""));
		_tcscpy(szSuite,_T(""));
		nStrID			= -1;
*/
		_tcscpy(szSuitePath,_T(""));
/*
		nCheck			= -1;
		nCounter		= 0;
		_tcscpy(szItemText,_T(""));
*/
  }
//  CShellTreeInfo(LPCTSTR caption,LPCTSTR icon,LPCTSTR node_name,LPCTSTR element,short visible,int idx,LPCTSTR func,
//						LPCTSTR suite,int strid,LPCTSTR suite_path,short check,long counter,LPCTSTR item_text)
	CShellTreeInfo(LPCTSTR caption,LPCTSTR suite_path)
    {
		_tcscpy(szCaption,caption);
/*
		_tcscpy(szIcon,icon);
		_tcscpy(szNodeName,node_name);
		_tcscpy(szElement,element);
		nVisible		= visible;
		nIndex			= idx;
		_tcscpy(szFunc,func);
		_tcscpy(szSuite,suite);
		nStrID			= strid;
*/
		_tcscpy(szSuitePath,suite_path);
/*
		nCheck			= check;
		nCounter		= counter;
		_tcscpy(szItemText,_T(item_text));
*/
  }
  CShellTreeInfo(const CShellTreeInfo &c)
  {
		*this = c;
  }
	LPCTSTR getCaption()			{ return szCaption; }
/*
	LPCTSTR getIcon()				{ return szIcon; }
	LPCTSTR getNodeName()	  { return szNodeName; }
	LPCTSTR getElement()			{ return szElement; }
	short getVisible()			{ return nVisible; }
	void setVisible(short v) { nVisible = v;	}
	int getIndex()					{ return nIndex; }
	LPCTSTR getFunc()				{ return szFunc; }
	LPCTSTR getSuite()				{ return szSuite; }
	int getStrID()					{ return nStrID; }
*/
	LPCTSTR getSuitePath()		{ return szSuitePath; }
/*
	short getCheck()				{ return nCheck; }
	long getCounter()				{ return nCounter; }
	LPCTSTR getItemText()		{ return szItemText; }
	void setItemtext(LPCTSTR s) { _tcscpy(szItemText,s);	}
*/
};

typedef std::vector<CShellTreeInfo> vecShellTree;

//////////////////////////////////////////////////////////////////////////////////
// Read new bulletine RSS file from WEB; 060809 p�d

BOOL getRSSXMLFile(CStringArray &str_array,long *size);


//////////////////////////////////////////////////////////////////////////////////
//	CNewsBulletineItem
class CNewsBulletineItem
{
//private:
	CString m_sTitle;
	CString m_sLink;
	CString m_sDescription;
	CString m_sCategory;
	CString m_sPubDate;
public:
	// Default contructor
	CNewsBulletineItem(void)
	{
		m_sTitle				= _T("");
		m_sLink					= _T("");	
		m_sDescription	= _T("");
		m_sCategory			= _T("");
		m_sPubDate			= _T("");
	}
	CNewsBulletineItem(LPCTSTR title,LPCTSTR link,LPCTSTR desc,LPCTSTR category,LPCTSTR pub_date)
	{
		m_sTitle				= (title);
		m_sLink					= (link);	
		m_sDescription	= (desc);
		m_sCategory			= (category);
		m_sPubDate			= (pub_date);
	}
	// Copy contructor
	CNewsBulletineItem(const CNewsBulletineItem &c)
	{
		*this = c;
	}

	CString getTitle(void)			{ return m_sTitle; }
	CString getLink(void)				{ return m_sLink; }
	CString getDesc(void)				{ return m_sDescription; }
	CString getCategory(void)		{ return m_sCategory; }
	CString getPubDate(void)		{ return m_sPubDate; }
};

typedef std::vector<CNewsBulletineItem> vecNewsBulletineItems;

//////////////////////////////////////////////////////////////////////////////////
//	RSSXMLFilePars
class RSSXMLFilePars
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	RSSXMLFilePars(void);

	virtual ~RSSXMLFilePars();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL getThisPubDate(LPTSTR);

	BOOL getNewsBulletines(vecNewsBulletineItems &);

	BOOL getXML(CString &xml);

};


//////////////////////////////////////////////////////////////////////////
// This class is a customization of standard Report Paint Manager, 
// which allows drawing record items text with word wrapping.
// You can test this customization sample with "Multiline Sample" menu option.
// It is implemented using DT_WORDBREAK mode, and uses 2 methods overriding.
class CReportMultilinePaintManager : public CXTPReportPaintManager
{
public:
	CReportMultilinePaintManager();
	virtual ~CReportMultilinePaintManager();

	// Draws Item Caption with word wrapping.
	void DrawItemCaption(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pMetrics);

	// Customized calculation of the row height in word wrapping mode, 
	// which is required in other report drawing methods.
	int GetRowHeight(CDC* pDC, CXTPReportRow* pRow, int nWidth);

};

/////////////////////////////////////////////////////////////////////////////
// CUserReportsRec

class CUserReportsRec : public CXTPReportRecord
{
	CString sSearchPath;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CUserReportsRec(void)
	{
		sSearchPath.Empty();
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CUserReportsRec(LPCTSTR title,LPCTSTR comment,LPCTSTR key_word,LPCTSTR head_line,LPCTSTR subject,LPCTSTR file_path,LPCTSTR created)
	{
		sSearchPath = file_path;
		AddItem(new CTextItem(title));
		AddItem(new CTextItem(comment));
		AddItem(new CTextItem(key_word));
		AddItem(new CTextItem(head_line));
		AddItem(new CTextItem(subject));
		AddItem(new CTextItem(extractFileName(file_path)));
		AddItem(new CTextItem(created));
	}

	CString getSearchPath(void)	{ return sSearchPath; }

	CString getColumnText(int item)	{ return ((CTextItem*)GetItem(item))->getTextItem();	}
	
	void setColumnText(int item,LPCTSTR text)	{ ((CTextItem*)GetItem(item))->setTextItem(text);	}
};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions
CString getResStr(UINT id);

//CString getModulesDir(void);
//CString getModuleFN(HINSTANCE hinst);

//CString getLanguageDir(void);
//CString getLangSet();

CString getNewsCategoriesSetupPathAndFile(void);

CString getShellDataDir(void);
BOOL getShellDataFiles(CStringArray &str_list);
BOOL setShellDataFiles(CStringArray &str_list);

BOOL getShellTreeInfo(CStringArray &,	// Array of files in Shortcuts.xml
										  vecShellTree &);		// ShellTree data

CString getToolBarResourceFN(void);

CWnd *getFormViewParentByID(int idd);
CView *getFormViewByID(int idd);
