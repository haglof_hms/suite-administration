//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Adminstration.rc
//
#define IDD_FORMVIEW                    1500
#define IDD_FORMVIEW1                   1501
#define IDD_FORMVIEW2                   1502
#define IDD_DIALOGBAR1                  1503
#define IDD_FILTEREDIT1                 1504
#define ID_USER_REPORT									1505
#define IDD_FIELD_CHOOSER               1506
#define IDC_COLUMNLIST1_1               8065
#define IDC_WORKSPACE                   10008
#define IDI_FORMICON                    24000
#define IDC_BTN_MOVE_DOWN               24002
#define IDR_TOOLBAR1                    24003
#define IDC_LIST1                       24004
#define IDC_GROUP                       24006
#define IDC_LIST2                       24007
#define IDC_BTN_MOVE_UP                 24008
#define IDC_GROUP1                      24009
#define IDC_STATIC1                     24010
#define IDC_EDIT1                       24011
#define IDC_FILTEREDIT1                 24012
#define IDC_LBL1_1                      24013
#define IDC_LBL1_2                      24014
#define ID_TBBTN_FILTER                 33771
#define ID_TBBTN_FILTER_OFF             33772
#define ID_TBBTN_IMPORT                 33773
#define ID_TBBTN_PRINT                  33774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        24004
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         24014
#define _APS_NEXT_SYMED_VALUE           24000
#endif
#endif
