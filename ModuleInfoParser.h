class CAdminSuiteModuleInfoParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	MSXML2::IXMLDOMNodePtr addItem(MSXML2::IXMLDOMNodePtr,LPCTSTR);
public:
	CAdminSuiteModuleInfoParser(void);

	virtual ~CAdminSuiteModuleInfoParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL CreateDoc(void);

	BOOL setSuiteModuleInfo(vecINFO_TABLE &);

	BOOL getSuiteModuleInfo(vecINFO_TABLE &);

	BOOL getXML(CString &xml);
};
