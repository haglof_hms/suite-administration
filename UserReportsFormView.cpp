#include "stdafx.h"
#include "MDIBaseFrame.h"
#include "UserReportsFormView.h"
#include "SelectDialog.h"
#include <shellapi.h>


#define BUFSIZE  512

/////////////////////////////////////////////////////////////////////////////
// CPropertyReportFilterEditControl

IMPLEMENT_DYNCREATE(CReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CUserReportsFormView

IMPLEMENT_DYNCREATE(CUserReportsFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CUserReportsFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(MSG_OPEN_SUITE_ARG, OnOpenSuiteArgMessage)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_COMMAND(ID_TBBTN_COLUMNS, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_PRINT, OnPrintOutReport)
	ON_COMMAND(ID_TBBTN_IMPORT, OnImportReport)
END_MESSAGE_MAP()

CUserReportsFormView::CUserReportsFormView()
	: CXTPReportView()
{
	m_nSelectedColumn = -1;
	m_handleReports = NO_VALUE;
}

CUserReportsFormView::~CUserReportsFormView()
{
}

void CUserReportsFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

//	getReportsFromDisk();

	setupReport();

	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST1_1, &pWnd->m_wndFieldChooser);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT1, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl1_2.GetSafeHwnd() == NULL)
	{
		m_wndLbl1_2.SubclassDlgItem(IDC_LBL1_2, &pWnd->m_wndFilterEdit);
		m_wndLbl1_2.SetBkColor(INFOBK);
	}

	if (m_wndLbl1_1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1_1.SubclassDlgItem(IDC_LBL1_1, &pWnd->m_wndFilterEdit);
		m_wndLbl1_1.SetBkColor(INFOBK);
		m_wndLbl1_1.SetLblFont(14,FW_BOLD);
	}

	LoadReportState();
}

void CUserReportsFormView::OnDestroy()
{
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CUserReportsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CUserReportsFormView diagnostics

#ifdef _DEBUG
void CUserReportsFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CUserReportsFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CUserReportsFormView message handlers

// CUserReportsFormView message handlers
void CUserReportsFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CUserReportsFormView::OnSetFocus(CWnd*)
{
	updateToolbarButtons();
}

void CUserReportsFormView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPReportView::OnShowWindow(bShow,nStatus);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CUserReportsFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_DELETE_ITEM :
		{
			removeReportFromDisk();
			break;
		}	// case ID_DELETE_ITEM :
	};

	return 0L;
}

LRESULT CUserReportsFormView::OnOpenSuiteArgMessage(WPARAM wParam,LPARAM lParam)
{
	CString sArgument((LPCTSTR)lParam);

	if (sArgument.CompareNoCase(IDENTITY_FOR_LOGSCALE_ARG) == 0)
	{
		m_handleReports = ARG_TYPES::LOGSCALE_REPORTS;
	}
	else if (sArgument.CompareNoCase(IDENTITY_FOR_FOREST_ARG) == 0)
	{
		m_handleReports = ARG_TYPES::FOREST_REPORTS;
	}
	else if (sArgument.CompareNoCase(IDENTITY_FOR_TIMBERCRUISE_ARG) == 0 || sArgument.CompareNoCase(IDENTITY_FOR_TIMBERCRUISE_ARG2) == 0)
	{
		m_handleReports = ARG_TYPES::TIMBERCRUISE_REPORTS;
	}
	else if (sArgument.CompareNoCase(IDENTITY_FOR_ESTIMATE_ARG) == 0)
	{
		m_handleReports = ARG_TYPES::ESTIMATE_REPORTS;
	}
	else if (sArgument.CompareNoCase(IDENTITY_FOR_LANDVALUE_ARG) == 0)
	{
		m_handleReports = ARG_TYPES::LANDVALUE_REPORTS;
	}
	else
	{
		m_handleReports = NO_VALUE;
	}

	getReportsFromDisk();

	return 0L;
}

// Create and add Assortment settings reportwindow
BOOL CUserReportsFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN)) 
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sGroupByThisField	= xml.str(IDS_STRING242);
			m_sGroupByBox		= xml.str(IDS_STRING243);
			m_sFieldChooser		= xml.str(IDS_STRING244);
			m_sFilterOn			= xml.str(IDS_STRING241);

			// Get text from languagefile; 061207 p�d
			if (GetReportCtrl().GetSafeHwnd() != NULL)
			{
				GetReportCtrl().ShowGroupBy(TRUE);
				GetReportCtrl().ShowWindow( SW_NORMAL );
				// Add these 3 lines to add scrollbars for View; 070319 p�d
				GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
				GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
				GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

				// "Titel"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING200), 200));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK );
				// "Kommentarer"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING201), 200));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK);
				// "Nyckelord"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING202), 150));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK );
				// "Rubrik"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING203), 100));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK );
				// "�mne"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING204), 100));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK );
				// "Filnamn"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING205), 150));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK );
				// "Skapad"
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING206), 50));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT | DT_WORDBREAK );

				GetReportCtrl().AllowEdit(FALSE);
	
				GetReportCtrl().SetPaintManager(new CReportMultilinePaintManager());
				GetReportCtrl().EnableToolTips(FALSE);

				GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				GetReportCtrl().GetPaintManager()->m_bUseColumnTextAlignment = TRUE;

				GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				
				GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);

			}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			xml.clean();
		}
	}

	return TRUE;
}

void CUserReportsFormView::setFilterWindow(void)
{
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_wndLbl1_1.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1_2.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
}


void CUserReportsFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}
	// Check if Fileter window is visible. If so change filter column to selected column; 090224 p�d
	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)
	pWnd = NULL;
}

void CUserReportsFormView::OnReportItemDblClick(NMHDR* pNotifyStruct, LRESULT* result)
{
	OnPrintOutReport();
}

void CUserReportsFormView::OnShowFieldChooser()
{
	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooser.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooser, bShow, FALSE);
	}	// if (pWnd != NULL)
}

void CUserReportsFormView::OnShowFieldFilter()
{
	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CUserReportsFormView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}	// if (pWnd != NULL)
}

void CUserReportsFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}
}

// CUserReportsFormView message handlers

void CUserReportsFormView::OnPrintOutReport(void)
{
	CString csReport;
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();

	if(pRow != NULL)
	{
		CUserReportsRec* pRec = (CUserReportsRec*)pRow->GetRecord();
		csReport.Format(_T("%s"), pRec->getSearchPath());
		if (fileExists(csReport))
		{
			AfxGetApp()->BeginWaitCursor();
			// tell the report-suite to open the report
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,(LPARAM)&_user_msg(333,_T("OpenSuiteEx"),_T("Reports2.dll"),csReport,_T(""),_T("")));
			AfxGetApp()->EndWaitCursor();
		}
	}//if(pRow != NULL)
}

void CUserReportsFormView::OnImportReport()
{
	HKEY hKey;
	CString S;
	CString sCopyTo,sCopyFrom;
	TCHAR szDir[BUFSIZE];
	DWORD dwDirSize = BUFSIZE;

	// Create buffer for file names.
	const DWORD numberOfFileNames = 200;
	const DWORD fileNameMaxLength = MAX_PATH + 1;
	const DWORD bufferSize = (numberOfFileNames * fileNameMaxLength) + 1;
	TCHAR* filenamesBuffer = new TCHAR[bufferSize];
	// szFilters is a text string for file name filter
	TCHAR szFilters[]= _T("CrystalReports (*.rpt)|*.rpt||");
	
	//#4495 20150924 J�
	LPCTSTR strRegFolder=_T("");
	LPCTSTR strRepFolder=_T("");
	switch(m_handleReports)
	{
	case NO_VALUE:
		strRepFolder=USER_REPORTS_DIR;
		break;
	case ARG_TYPES::LOGSCALE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_LOGSCALE;
		strRepFolder=USER_REPORTS_DIR_LOGSCALE;
		break;
	case ARG_TYPES::TIMBERCRUISE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_TCRUISE;
		strRepFolder=USER_REPORTS_DIR_TIMBERCRUISE;
		break;
	case ARG_TYPES::FOREST_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_FOREST;
		strRepFolder=USER_REPORTS_DIR_FOREST;
		break;
	case ARG_TYPES::ESTIMATE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_ESTIMATE;
		strRepFolder=USER_REPORTS_DIR_ESTIMATE;
		break;
	case ARG_TYPES::LANDVALUE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_LANDVALUE;
		strRepFolder=USER_REPORTS_DIR_LANDVALUE;
		break;
	}

	switch(m_handleReports)
	{
	case NO_VALUE:
		sCopyTo.Format(_T("%s%s\\"), getPathToCommonAppData(), strRepFolder);
		break;
	case ARG_TYPES::LOGSCALE_REPORTS:
	case ARG_TYPES::TIMBERCRUISE_REPORTS:
	case ARG_TYPES::FOREST_REPORTS:
	case ARG_TYPES::ESTIMATE_REPORTS:
	case ARG_TYPES::LANDVALUE_REPORTS:
		//get directory from registry
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
		{
			if(RegQueryValueEx(hKey, strRegFolder/*HMS_INST_DIR_KEY*/, NULL, NULL, (LPBYTE)szDir, &dwDirSize) == ERROR_SUCCESS)
			{
				sCopyTo.Format(_T("%s\\"), szDir);
			}
			else
				sCopyTo.Format(_T("%s%s\\"), getPathToCommonAppData(), strRepFolder);
			RegCloseKey(hKey);
		}
		else
			sCopyTo.Format(_T("%s%s\\"), getPathToCommonAppData(), strRepFolder);
		break;
	}


	
	// Create dialog to open multiple files.
	CFileDialog dlg(TRUE, _T("rpt"), _T("*.rpt"), OFN_ALLOWMULTISELECT | OFN_HIDEREADONLY, szFilters, this);

	// Initialize beginning and end of buffer.
	filenamesBuffer[0] = NULL;
	filenamesBuffer[bufferSize-1] = NULL;

	// Attach buffer to OPENFILENAME member.
	dlg.m_ofn.lpstrFile = filenamesBuffer;
	dlg.m_ofn.nMaxFile = bufferSize;

	// Create array for file names.
	CStringArray fileNameArray;
	if(dlg.DoModal() == IDOK)
	{
		// Retrieve file name(s).
		POSITION fileNamesPosition = dlg.GetStartPosition();
		while(fileNamesPosition != NULL)
		{
			fileNameArray.Add(dlg.GetNextPathName(fileNamesPosition));
		}  
		// Check if there's any files to copy from; 090518 p�d
		if (fileNameArray.GetCount() > 0)
		{
			for (int i = 0;i < fileNameArray.GetCount();i++)
			{
				sCopyFrom = fileNameArray[i];
				if (fileExists(sCopyFrom))
				{
					copyFile(sCopyFrom,sCopyTo);
				}	// if (fileExists(sCopyFrom))
			}	// if (fileExists(sCopyFrom))
			getReportsFromDisk();
		}	// if (fileNameArray.GetCount() > 0)
	}
	// Release file names buffer.
	delete[] filenamesBuffer;
	// Clear filename array; 090518 p�d
	fileNameArray.RemoveAll();
}

void CUserReportsFormView::updateToolbarButtons()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,(GetReportCtrl().GetRows()->GetCount() > 0));

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CUserReportsFormView::removeReportFromDisk()
{
	CString csReport;
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();

	if(pRow != NULL)
	{
		CUserReportsRec* pRec = (CUserReportsRec*)pRow->GetRecord();
		csReport.Format(_T("%s"), pRec->getSearchPath());
		if (fileExists(csReport))
		{
			SHFILEOPSTRUCT shop;
			memset(&shop, 0, sizeof(SHFILEOPSTRUCT));
			shop.wFunc	= FO_DELETE;
			shop.pFrom	= csReport + '\0'; // We need to add a second null-char
			shop.fFlags	= FOF_ALLOWUNDO;
			if( SHFileOperation(&shop) == 0 )
			{
				getReportsFromDisk();
			}

			updateToolbarButtons();
		}	// if (fileExists(csReport))
	}//if(pRow != NULL)
}


BOOL CUserReportsFormView::getReportsFromDisk(void)
{
	HKEY hKey;
	CString S;
	CString sPath,sDir,sTitle,sComment,sSubject,sAuthor,sKeywords,sCreated;
	TCHAR szDir[BUFSIZE];
	DWORD dwDirSize = BUFSIZE;

	m_sUserReports.RemoveAll();	// Clear; 090515 p�d

	CString sUserReportsDir;

	//#4495 20150924 J�
	LPCTSTR strRegFolder=_T("");
	LPCTSTR strRepFolder=_T("");
	switch(m_handleReports)
	{
	case NO_VALUE:
		strRepFolder=USER_REPORTS_DIR;
		break;
	case ARG_TYPES::LOGSCALE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_LOGSCALE;
		strRepFolder=USER_REPORTS_DIR_LOGSCALE;
		break;
	case ARG_TYPES::TIMBERCRUISE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_TCRUISE;
		strRepFolder=USER_REPORTS_DIR_TIMBERCRUISE;
		break;
	case ARG_TYPES::FOREST_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_FOREST;
		strRepFolder=USER_REPORTS_DIR_FOREST;
		break;
	case ARG_TYPES::ESTIMATE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_ESTIMATE;
		strRepFolder=USER_REPORTS_DIR_ESTIMATE;
		break;
	case ARG_TYPES::LANDVALUE_REPORTS:
		strRegFolder=REPORTS_INST_DIR_KEY_LANDVALUE;
		strRepFolder=USER_REPORTS_DIR_LANDVALUE;
		break;
	}

	switch(m_handleReports)
	{
	case NO_VALUE:
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), strRepFolder);
		break;
	case ARG_TYPES::LOGSCALE_REPORTS:
	case ARG_TYPES::TIMBERCRUISE_REPORTS:
	case ARG_TYPES::FOREST_REPORTS:
	case ARG_TYPES::ESTIMATE_REPORTS:
	case ARG_TYPES::LANDVALUE_REPORTS:
		//get directory from registry
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
		{
			if(RegQueryValueEx(hKey, strRegFolder/*HMS_INST_DIR_KEY*/, NULL, NULL, (LPBYTE)szDir, &dwDirSize) == ERROR_SUCCESS)
			{
				sUserReportsDir.Format(_T("%s\\"), szDir);
			}
			else
				sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), strRepFolder);
			RegCloseKey(hKey);
		}
		else
			sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), strRepFolder);
		break;
	}


/*
	if (m_handleReports == NO_VALUE)
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), USER_REPORTS_DIR);
	else if (m_handleReports == ARG_TYPES::LOGSCALE_REPORTS)
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), USER_REPORTS_DIR_LOGSCALE);
	else if (m_handleReports == ARG_TYPES::TIMBERCRUISE_REPORTS)
	{
#ifdef _USE_REGISTRY_PATH
		//get directory from registry
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
		{
			if(RegQueryValueEx(hKey, REPORTS_INST_DIR_KEY, NULL, NULL, (LPBYTE)szDir, &dwDirSize) == ERROR_SUCCESS)
			{
				sUserReportsDir.Format(_T("%s\\"), szDir);
			}
			RegCloseKey(hKey);
		}
#else
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), USER_REPORTS_DIR_TIMBERCRUISE);
#endif
	}
	else if (m_handleReports == ARG_TYPES::FOREST_REPORTS)
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), USER_REPORTS_DIR_FOREST);
	else if (m_handleReports == ARG_TYPES::ESTIMATE_REPORTS)
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), USER_REPORTS_DIR_ESTIMATE);
	else if (m_handleReports == ARG_TYPES::LANDVALUE_REPORTS)
		sUserReportsDir.Format(_T("%s%s\\"), getPathToCommonAppData(), USER_REPORTS_DIR_LANDVALUE);
		*/

	getListOfFilesInDirectory(_T("*.rpt"),sUserReportsDir,m_sUserReports,EF_FULLY_QUALIFIED);

	GetReportCtrl().ResetContent();	// Clear
	if (m_sUserReports.GetCount() > 0)
	{
		for (short i = 0;i < m_sUserReports.GetCount();i++)
		{
			sPath = m_sUserReports.GetAt(i);
			sTitle = getFileInfo(sPath, 0x00000002);	//title
			sSubject = getFileInfo(sPath, 0x00000003);	//Subject
			sAuthor = getFileInfo(sPath, 0x00000004);		//author
			sKeywords = getFileInfo(sPath, 0x00000005);	//keywords
			sComment = getFileInfo(sPath, 0x00000006);	//comment
			sCreated = getFileInfo(sPath, 0x0000000D);	//created

			GetReportCtrl().AddRecord(new CUserReportsRec(sTitle,sComment,sKeywords,sAuthor,sSubject,sPath,sCreated));
		}
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();

	}	// if (m_sUserReports.GetCount() > 0)

	return TRUE;
}

CString CUserReportsFormView::getFileInfo(LPCTSTR path, PROPID propid)
{
	
	CString csOut = _T("");

	//WCHAR wPath[BUFSIZ];

//	MultiByteToWideChar(CP_ACP, 0, path, (int)(strlen(path)+1), wPath, sizeof(wPath)/sizeof(wPath[0]));
	
	IPropertySetStorage *pPropSetStg = NULL;
	IPropertyStorage *pPropStg = NULL;
	PROPSPEC propspec; 
	PROPVARIANT propRead;
	HRESULT hr = S_OK;
	
	hr = ::StgOpenStorageEx(/*wPath*/path,
		STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,	
		STGFMT_ANY,
		0,
		NULL,
		NULL,
		IID_IPropertySetStorage,
		reinterpret_cast<void**>(&pPropSetStg) );

	if(hr != S_OK)
	{
		return _T("");
	}


	hr = pPropSetStg->Open( PropSetfmtid, 
                            STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,
                            &pPropStg );

	if(hr != S_OK)
	{
		pPropSetStg->Release();
		return _T("");
	}

	propspec.ulKind = PRSPEC_PROPID;
	propspec.propid  = propid;	

	hr = pPropStg->ReadMultiple( 1, &propspec, &propRead );
	
	if(hr != S_OK)
	{
		pPropStg->Release();
		pPropSetStg->Release();
		return _T("");
	}

	if(propid >= 0x0000000A && propid <= 0x0000000D)
	{
		FILETIME ft;
		SYSTEMTIME st;
		ft = propRead.filetime;
		FileTimeToSystemTime(&ft, &st);
// American date
//		csOut.Format(_T("%02d/%02d/%4d"), st.wMonth, st.wDay, st.wYear);
// Swedish date
		csOut.Format(_T("%4d-%02d-%02d %02d:%02d"), st.wYear,st.wMonth, st.wDay, st.wHour,st.wMinute);
	}
	else
	{
		//TODO: might need to change this
		int nWlen = (int)wcslen(propRead.pwszVal);
		int nLen = (int)strlen(propRead.pszVal);
		if(nLen > 1 && nWlen > 1)
			csOut.Format(_T("%S"), propRead.pszVal);
		else
			csOut.Format(_T("%s"), propRead.pwszVal);

		//check if string length is more then one character, else asume multiple nul in string
/*		if(strlen(propRead.pszVal) <= 2)
		{
			csOut = propRead.pwszVal;
			if(csOut.Find(_T("??")) >= 0)	//check just in case
			{
				csOut.Format(_T("%s"), propRead.pwszVal);
			}
		}
		else
		{
			csOut.Format(_T("%s"), propRead.pwszVal);
		}

		*/
	}


	pPropStg->Release();
	pPropSetStg->Release();
	
	return csOut;
}


void CUserReportsFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_USERREPORT_SEL_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString((REG_WP_USERREPORT_SEL_KEY), _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt((REG_WP_USERREPORT_SEL_KEY), _T("SelColIndex"),0);
	sFilterText = AfxGetApp()->GetProfileString((REG_WP_USERREPORT_SEL_KEY), _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt((REG_WP_USERREPORT_SEL_KEY), _T("SelColIndex"),0);
	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	CUserReportsFrame* pWnd = (CUserReportsFrame *)getFormViewParentByID(ID_USER_REPORT);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
}

void CUserReportsFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_USERREPORT_SEL_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString((REG_WP_USERREPORT_SEL_KEY), _T("FilterText"), sFilterText);

	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt((REG_WP_USERREPORT_SEL_KEY), _T("SelColIndex"), m_nSelectedColumn);

}

