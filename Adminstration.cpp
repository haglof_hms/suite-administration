// Adminstration.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "MDIBaseFrame.h"
#include "Administration.h"

#include "NewsBulletineDlg.h"
#include "NavigationPaneDialog.h"
#include "SuitesUserModulesDlg.h"

#include "UserReportsFormView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines


std::vector<HINSTANCE> m_vecHInstTable;

static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

static AFX_EXTENSION_MODULE AdministrationDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInst = hInstance;
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
	
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(AdministrationDLL, hInstance))
			return 0;

	}		
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		// Terminate the library before destructors are called
		AfxTermExtensionModule(AdministrationDLL);
	}
	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(AdministrationDLL);

	CString sModuleFN = getModuleFN(hInst);
	// Setup the language filename
	CString sLangFN;
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	m_vecHInstTable.clear();

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDINewsBulletineFrame),
			RUNTIME_CLASS(CNewsBulletineDlg)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDINavigationWindowFrame),
			RUNTIME_CLASS(CNavigationPaneDialog)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW1,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDISuitesUserModulesFrame),
			RUNTIME_CLASS(CSuitesUserModulesDlg)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW2,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(ID_USER_REPORT,	
			RUNTIME_CLASS(CMDIReportDoc),
			RUNTIME_CLASS(CUserReportsFrame),
			RUNTIME_CLASS(CUserReportsFormView)));
	vecIndex.push_back(INDEX_TABLE(ID_USER_REPORT,sModuleFN,sLangFN,TRUE));

	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,1 /* Suite */,
									(TCHAR*)sLangFN.GetBuffer(),
									(TCHAR*)sVersion.GetBuffer(),
									(TCHAR*)sCopyright.GetBuffer(),
									(TCHAR*)sCompany.GetBuffer()));

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */

	typedef CRuntimeClass *(*Func)(CWinApp *,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
  Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"),getModulesDir(),user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
#ifdef UNICODE
				USES_CONVERSION;
				proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], W2A(INIT_MODULE_FUNC) );
#else
				proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], (INIT_MODULE_FUNC) );
#endif
					if (proc != NULL)
					{
						// call the function
						proc(pApp,sModuleFN,vecIndex,vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)

}

void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sDocTitle;
	CString S;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;


	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;
			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	// Make sure the languagefile is in directory; 081215 p�d
	if (!fileExists(sLangFN))
	{
		S.Format(_T("NB! Languagefile missing for suite/module:\n%s\n\nCan not open ..."),
			sVecIndexTableModuleFN);
		AfxMessageBox(S);
		return;
	}

	if (bFound)
	{
		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;

		// Check if the document or module is in this SUITE; 051213 p�d
		POSITION pos = pApp->GetFirstDocTemplatePosition();
		while(pos != NULL)
		{
			pTemplate = pApp->GetNextDocTemplate(pos);
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			ASSERT(pTemplate != NULL);
			// Need to add a linefeed, infront of the docName.
			// This is because, for some reason, the document title,
			// set in resource, must have a linefeed.
			// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
			sDocName = '\n' + sDocName;

			if (pTemplate && sDocName.Compare(sResStr) == 0)
			{
				
					if (bIsOneInst)
					{
						POSITION posDOC = pTemplate->GetFirstDocPosition();

						while(posDOC != NULL)
						{
							CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
							sDocTitle.Format(_T("%s"),sCaption);
							pDocument->SetTitle(sDocTitle);
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								pView->GetParent()->BringWindowToTop();
								pView->GetParent()->SetFocus();
								posDOC = (POSITION)1;
								pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_ID_ARGUMENT,(LPARAM)(LPCTSTR)L"");
								break;
							}	// if(posView != NULL)
						}	// while(posDOC != NULL)

						if (posDOC == NULL)
						{

							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s"),sCaption);
								pDocument->SetTitle(sDocTitle);
								POSITION posView = pDocument->GetFirstViewPosition();
								if(posView != NULL)
								{
									CView* pView = pDocument->GetNextView(posView);
									pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_ID_ARGUMENT,(LPARAM)(LPCTSTR)L"");
								}

							}

							break;
						}
					}	// if (bIsOneInst)
					else
					{
							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							nDocCounter = 1;

							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
								pDocument->SetTitle(sDocTitle);
								nDocCounter++;
								POSITION posView = pDocument->GetFirstViewPosition();
								if(posView != NULL)
								{
									CView* pView = pDocument->GetNextView(posView);
									pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_ID_ARGUMENT,(LPARAM)(LPCTSTR)L"");
								}

							}

							break;
					}
			}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
		}	// while(pos != NULL)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}

}

void DLL_BUILD OpenSuiteArg(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,LPCTSTR arg1, int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sArgCap;
	CString sArgument(arg1);
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;
	CString sDocTitle;
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);

			// Check argument set in shelldata-file
			if (sArgument.CompareNoCase(IDENTITY_FOR_LOGSCALE_ARG) == 0)
			{
				sArgCap = xml->str(IDS_STRING10000);
				sDocTitle.Format(L"%s - %s",sCaption,sArgCap);
			}
			else if (sArgument.CompareNoCase(IDENTITY_FOR_FOREST_ARG) == 0)
			{
				sArgCap = xml->str(IDS_STRING10001);
				sDocTitle.Format(L"%s - %s",sCaption,sArgCap);
			}
			else if (sArgument.CompareNoCase(IDENTITY_FOR_TIMBERCRUISE_ARG) == 0 || sArgument.CompareNoCase(IDENTITY_FOR_TIMBERCRUISE_ARG2) == 0)
			{
				sArgCap = xml->str(IDS_STRING10002);
				sDocTitle.Format(L"%s - %s",sCaption,sArgCap);
			}
			else if (sArgument.CompareNoCase(IDENTITY_FOR_ESTIMATE_ARG) == 0)
			{
				sArgCap = xml->str(IDS_STRING10003);
				sDocTitle.Format(L"%s - %s",sCaption,sArgCap);
			}
			else if (sArgument.CompareNoCase(IDENTITY_FOR_LANDVALUE_ARG) == 0)
			{
				sArgCap = xml->str(IDS_STRING10004);
				sDocTitle.Format(L"%s - %s",sCaption,sArgCap);
			}
			else
			{
				sDocTitle.Format(L"%s",sCaption);
			}

		}
		delete xml;
		// Check if the document or module is in this SUITE; 051213 p�d
		POSITION pos = pApp->GetFirstDocTemplatePosition();
		while(pos != NULL)
		{
			pTemplate = pApp->GetNextDocTemplate(pos);
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			ASSERT(pTemplate != NULL);
			// Need to add a linefeed, infront of the docName.
			// This is because, for some reason, the document title,
			// set in resource, must have a linefeed.
			// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
			sDocName = '\n' + sDocName;

			if (pTemplate && sDocName.Compare(sResStr) == 0)
			{
				
				if (bIsOneInst)
				{
					POSITION posDOC = pTemplate->GetFirstDocPosition();

					while(posDOC != NULL)
					{
						CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
						if (pDocument != NULL)
						{
							pDocument->SetTitle(sDocTitle);
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								pView->GetParent()->BringWindowToTop();
								pView->GetParent()->SetFocus();
								posDOC = (POSITION)1;
								pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_ID_ARGUMENT,(LPARAM)(LPCTSTR)arg1);
								break;
							}	// if(posView != NULL)
						}	// if (pDocument != NULL)
					}	// while(posDOC != NULL)

					if (posDOC == NULL)
					{

						pTemplate->OpenDocumentFile(NULL);

						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						while (posDOC != NULL)
						{
							CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
							if (pDocument != NULL)
							{
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								pDocument->SetTitle(sDocTitle);
								POSITION posView = pDocument->GetFirstViewPosition();
								if(posView != NULL)
								{
									CView* pView = pDocument->GetNextView(posView);
									pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_ID_ARGUMENT,(LPARAM)(LPCTSTR)arg1);
								}
							}
						}

						break;
					}
				}	// if (bIsOneInst)
				else
				{
						pTemplate->OpenDocumentFile(NULL);

						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						nDocCounter = 1;

						while (posDOC != NULL)
						{
							CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
							if (pDocument != NULL)
							{
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
								pDocument->SetTitle(sDocTitle);
								nDocCounter++;
								POSITION posView = pDocument->GetFirstViewPosition();
								if(posView != NULL)
								{
									CView* pView = pDocument->GetNextView(posView);
									pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_ID_ARGUMENT,(LPARAM)(LPCTSTR)arg1);
								}
							}
						}

						break;
					}
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)

			*ret = 1;
		}	// if (bFound)
		else
		{
			*ret = 0;
		}
}
