#ifndef _USERREPORTSFORMVIEW_H_
#define _USERREPORTSFORMVIEW_H_
#pragma once

#include "Resource.h"

#include "ResLangFileReader.h"

//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d

class CReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CReportFilterEditControl)
public:
	CReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////////
// CUserReportsFormView form view

class CUserReportsFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CUserReportsFormView)

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

	CStringArray m_sUserReports;

	CString m_sLangFN;

	int m_handleReports;
protected:
	CUserReportsFormView();           // protected constructor used by dynamic creation
	virtual ~CUserReportsFormView();

	vecTransactionProperty m_vecPropertyData;
	BOOL setupReport(void);
	BOOL getReportsFromDisk(void);

	CXTPReportSubListControl m_wndSubList;
	CReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl1_1;
	CMyExtStatic m_wndLbl1_2;

	CString getFileInfo(LPCTSTR path, PROPID propid);

	void LoadReportState(void);
	void SaveReportState(void);

	int m_nDBIndex;

	void setFilterWindow(void);

	void updateToolbarButtons();
	void removeReportFromDisk();
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	int getSelectedColumn() { return m_nSelectedColumn; }

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnOpenSuiteArgMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintOutReport();
	afx_msg void OnImportReport();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif
