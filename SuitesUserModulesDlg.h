#pragma once


#include "Resource.h"

// CSuitesUserModulesDlg form view

class CSuitesUserModulesDlg : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSuitesUserModulesDlg)

	BOOL setupReport1(void);

	void getSuiteUserModuleConfig(void);

	BOOL populateReport(void);

protected:

	CSuitesUserModulesDlg();           // protected constructor used by dynamic creation
	virtual ~CSuitesUserModulesDlg();

	CMyReportCtrl m_wndReport1;

	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

		vecINFO_TABLE m_vecInfoTable;

public:
	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


