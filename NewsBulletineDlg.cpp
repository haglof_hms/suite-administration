// NewsBulletineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Administration.h"
#include "NewsBulletineDlg.h"

#include "ResLangFileReader.h"
#include ".\newsbulletinedlg.h"

////////////////////////////////////////////////////////////////////////////////////////////////
// CPropertyGridItemExchange

CPropertyGridItemExchange::CPropertyGridItemExchange(CString strCaption,CString *strRetVal)
	: CXTPPropertyGridItem(strCaption,_T("Olle"),strRetVal)
{
	m_nFlags = xtpGridItemHasExpandButton;
	m_sValue = _T("");
}

CPropertyGridItemExchange::~CPropertyGridItemExchange(void)
{
}

BOOL CPropertyGridItemExchange::OnDrawItemValue(CDC& dc, CRect rcValue)
{
	CRect rcText(rcValue);

	dc.DrawText( (GetValue()), rcText,  DT_SINGLELINE|DT_VCENTER);

	return TRUE;
}

void CPropertyGridItemExchange::OnInplaceButtonDown()
{
	m_sValue = _T("Test");
	OnValueChanged((m_sValue));
	((CWnd*)m_pGrid)->Invalidate(FALSE);
}


// CNewsBulletineDlg

IMPLEMENT_DYNCREATE(CNewsBulletineDlg, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CNewsBulletineDlg, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()

CNewsBulletineDlg::CNewsBulletineDlg()
	: CXTResizeFormView(CNewsBulletineDlg::IDD)
{
	m_bInitialized = FALSE;
}

CNewsBulletineDlg::~CNewsBulletineDlg()
{
}

void CNewsBulletineDlg::OnDestroy()
{
	saveNewsCatergoriesSetup();

	CXTResizeFormView::OnDestroy();
}

void CNewsBulletineDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP, m_wndGroup);
	
	DDX_Control(pDX, IDC_STATIC, m_wndLbl1);

	//}}AFX_DATA_MAP
}

void CNewsBulletineDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get the size of the place holder, this will be used when creating the grid.
	CWnd* pPlaceHolder = GetDlgItem(IDC_WORKSPACE);

	SetScaleToFitSize(CSize(90, 1));

	SetFlag(xtResizeNoTransparentGroup);

	m_sAbrevLangSet = getLangSet();

	// Vector holds info. on setting of News categories; 060809 p�d
	m_vecNewsCategories.clear();

	CRect rc;
	pPlaceHolder->GetWindowRect( &rc );
	ScreenToClient(&rc);
	if (!m_bInitialized)
	{

		if (m_wndPropertyGrid.Create(CRect(0,0,0,0), this, IDC_PROP_GRID))
		{
			m_wndPropertyGrid.SetOwner(this);
			m_wndPropertyGrid.SetVariableItemsHeight(TRUE);
			m_wndPropertyGrid.MoveWindow(rc);
			m_wndPropertyGrid.Invalidate(FALSE);
			m_wndPropertyGrid.ShowHelp( FALSE );
			m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		}
		m_bInitialized = TRUE;
	}
	setLanguage();

	// Get permanent categories, set in registry; 060809 p�d
	getPermanentCategoriesInReg(m_szPermanentCategories);
	getNewsCatergoriesSetup();
	setupCategories();
}

BOOL CNewsBulletineDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CNewsBulletineDlg::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}


// CNewsBulletineDlg diagnostics

#ifdef _DEBUG
void CNewsBulletineDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CNewsBulletineDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// PRIVATE
void CNewsBulletineDlg::setLanguage(void)
{
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING100));

			m_sTrueCap = xml->str(IDS_STRING106);
			m_sFalseCap = xml->str(IDS_STRING107);
		}
		delete xml;
	}


}

BOOL CNewsBulletineDlg::excludeCategory(LPCTSTR category)
{
	int nItems,nLen;
	splitInfo(m_szPermanentCategories,&nItems,&nLen,';');

	for (int i = 0;i < nItems;i++)
	{
		if (_tcscmp(category,split(m_szPermanentCategories,i,';')) == 0)
			return TRUE;
	}	// for (int i = 0;i < nSize;i++)
	return FALSE;
}

void CNewsBulletineDlg::saveNewsCatergoriesSetup(void)
{
	CString sCategory;
	int nLength = 0;
	BOOL bIsSet;
	m_vecNewsCategories.clear();
	CXTPPropertyGridItems *cats = m_wndPropertyGrid.GetCategories();
	CXTPPropertyGridItem *item;
	CXTPPropertyGridItems *childs;
	CXTPPropertyGridItem *childItem;
	
	if (cats->GetCount() > 0)
	{
		for (int i = 0;i < cats->GetCount();i++)
		{
			item = cats->GetAt(i);
			childs = item->GetChilds();
			if (childs->GetCount() > 0)
			{
				for (int j = 0;j < childs->GetCount();j++)
				{
					childItem = childs->GetAt(j);
					bIsSet = (_tcscmp(childItem->GetValue(),m_sTrueCap) == 0);
					sCategory.Format(_T("%s - %s"),item->GetCaption(),childItem->GetCaption());
					m_vecNewsCategories.push_back(CNewsCategoriesSetupItem(bIsSet,sCategory));
				}	// for (int j = 0;j < childs->GetCount();j++)
			}	// if (childs->GetCount() > 0)
		}	// for (int i = 0;i < cats->GetCount();i++)

		// save only if we have any categories
		saveNewsCatergoriesSetupInfo(m_vecNewsCategories);

	}	// if (cats->GetCount() > 0)
}

void CNewsBulletineDlg::getNewsCatergoriesSetup(void)
{
	// getNewsCatergoriesSetupInfo in HMSFuncLib; 060809 p�d
	getNewsCatergoriesSetupInfo(m_vecNewsCategories);
}

// PROTECTED

void CNewsBulletineDlg::OnPaint()
{
	CXTResizeFormView::OnPaint();
}

void CNewsBulletineDlg::setupCategories(void)
{
	CStringArray strArr;
	BOOL bFound;
	TCHAR szMainItem[128], szSubItem[128], szBuffer[128];
	// Vector holding News bulletins; 060802 p�d
	CNewsCategoriesSetupItem rec;

	// Add Categories to the ListBox
	if (m_vecNewsCategories.size() > 0)
	{
		for (UINT i = 0;i < m_vecNewsCategories.size();i++)
		{
			rec = m_vecNewsCategories[i];
			_tcscpy(szBuffer,rec.getCategory());
			// Check i a category is in the PERMANEMT_NEWS_CATEGORIES.
			// If so, the item is excluded from this list (can't be unselected); 060809 p�d
			_stscanf_s(szBuffer, _T("%[ a-zA-Z0-9������]-%[ a-zA-Z0-9������]"), szMainItem, 127, szSubItem, 127);
			_tcscpy(szMainItem, rtrim(szMainItem, ' '));	// trim trailing spaces
			_tcscpy(szSubItem, ltrim(szSubItem, ' '));	// trim leading spaces
			
			bFound = FALSE;
			// Check that the MainItem's not already in the strArr; 0609912 p�d
			for (int j = 0;j < strArr.GetCount();j++)
			{
				CString S;
				if (_tcscmp(szMainItem,(CString)strArr.GetAt(j)) == 0)
				{
					bFound = TRUE;
					break;
				}
			}
			if (!bFound)
				strArr.Add(szMainItem);

		}	// for (UINT i = 0;i < vecNewsBulletines.size();i++)

		if (strArr.GetCount() > 0)
		{
			for (int k = 0;k < strArr.GetCount();k++)
			{
				CXTPPropertyGridItem* pSettings  = m_wndPropertyGrid.AddCategory((CString)strArr.GetAt(k));

				for (UINT i = 0;i < m_vecNewsCategories.size();i++)
				{
					rec = m_vecNewsCategories[i];
					_tcscpy(szBuffer,rec.getCategory());
					// Check i a category is in the PERMANEMT_NEWS_CATEGORIES.
					// If so, the item is excluded from this list (can't be unselected); 060809 p�d
					_stscanf_s(szBuffer, _T("%[ a-zA-Z0-9������]-%[ a-zA-Z0-9������]"), szMainItem, 127, szSubItem, 127);
					_tcscpy(szMainItem, rtrim(szMainItem, ' '));	// trim trailing spaces
					_tcscpy(szSubItem, ltrim(szSubItem, ' '));	// trim leading spaces

					if (_tcscmp(szMainItem,(CString)strArr.GetAt(k)) == 0)
					{
						pSettings->AddChildItem(new CMyPropertyGridItemBool(m_sTrueCap,m_sFalseCap,szSubItem, rec.getDisplay()));
					}
				}	// for (UINT i = 0;i < vecNewsBulletines.size();i++)

				pSettings->Expand();
			}	// for (int k = 0;k < strArr.GetCount();k++)
		}	// if (strArr.GetCount() > 0)


	}	// if (vecNewsBulletines.size() > 0)
}

// trim trailing characters from string
TCHAR* CNewsBulletineDlg::rtrim(TCHAR* string, TCHAR junk)
{
	TCHAR* original = string + _tcslen(string);
	while(*--original == junk);
	*(original + 1) = '\0';
	return string;
}

// trim leading characters from string
TCHAR* CNewsBulletineDlg::ltrim(TCHAR *string, TCHAR junk)
{
	TCHAR* original = string;
	TCHAR *p = original;
	int trimmed = 0;
	do
	{
		if (*original != junk || trimmed)
		{
			trimmed = 1;
			*p++ = *original;
		}
	}
	while (*original++ != '\0');
	return string;
}